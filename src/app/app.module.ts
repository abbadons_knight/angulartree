import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {TreeModule} from 'angular-tree-component';
import {AppComponent} from './app.component';
import {NodeTreeComponent} from './nodetree/nodetree.component';
import {HeaderComponent} from './header/header.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NodeTreeComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    TreeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
