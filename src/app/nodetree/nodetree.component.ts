import {Component, OnInit, ViewChild} from '@angular/core';
import {ITreeOptions, KEYS, TREE_ACTIONS, TreeComponent, TreeModel, TreeNode} from 'angular-tree-component';
import * as _ from 'lodash';

@Component({
  selector: 'app-nodetree',
  templateUrl: './nodetree.component.html',
})
export class NodeTreeComponent implements OnInit {

  categoryName: string;
  categoryDescription: string;
  isSelected: boolean;

  @ViewChild(TreeComponent)
  tree: TreeComponent;

  nodes = [];
  options: ITreeOptions = {
    displayField: 'name',
    isExpandedField: 'expanded',
    idField: 'uuid',
    hasChildrenField: 'nodes',
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          this.onClick(tree, node, $event);
        },
        dblClick: (tree, node, $event) => {
          if (node.hasChildren) {
            TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
          }
        },
        contextmenu: (tree, node, $event) => {

      }
      },
      keys: {
        [KEYS.RIGHT]: TREE_ACTIONS.DRILL_DOWN,
        [KEYS.LEFT]: TREE_ACTIONS.DRILL_UP,
        [KEYS.DOWN]: TREE_ACTIONS.NEXT_NODE,
        [KEYS.UP]: TREE_ACTIONS.PREVIOUS_NODE
      }
    },
    nodeHeight: 23,
    allowDrag: (node) => {
      return true;
    },
    allowDrop: (node) => {
      return true;
    },
    useVirtualScroll: true,
    animateExpand: true,
    scrollOnActivate: true,
    animateSpeed: 30,
    animateAcceleration: 1.2,
    scrollContainer: document.body.parentElement
  };
  private showingBtns: boolean;
  tempname: string;
  tempdesc: string;
  constructor() {
  }

  onClick(tree, node, $event) {
    if (this.tree.treeModel.getActiveNode() != null && this.tree.treeModel.getActiveNode() === node) {
      this.tree.treeModel.setActiveNode(this.tree.treeModel.getActiveNode(), false);
      this.categoryName = '';
      this.categoryDescription = '';
      this.isSelected = false;
    } else {
      TREE_ACTIONS.ACTIVATE(tree, node, $event);
      this.tempname = node.data.name;
      this.categoryName = this.tempname;
      this.tempdesc = node.data.description;
      this.categoryDescription = this.tempdesc;
      this.isSelected = true;
    }
    this.showBtns();
  }
  ngOnInit(): void {
    this.nodes = JSON.parse(localStorage.getItem('nodes'));
    this.tree.treeModel.nodes = JSON.parse(localStorage.getItem('nodes'));
  }
  onInit() {
    if (localStorage.treeState) {
      this.tree.treeModel.setState(JSON.parse(localStorage.treeState));
    }
    function subscription(newstate) {
      localStorage.treeState = JSON.stringify(newstate);
    }
    this.tree.treeModel.subscribeToState(subscription(this.getState()));
    console.log('Displaying ' + localStorage.getItem('nodes'));
  }

  ngAfterInit() {
    const treeModel: TreeModel = this.tree.treeModel;
    const firstNode: TreeNode = treeModel.getFirstRoot();

    firstNode.setActiveAndVisible();
  }

  getState() {
    return localStorage.treeState && JSON.parse(localStorage.treeState);
  }
  setState(state) {
    localStorage.treeState = JSON.stringify(state);
  }

  getId() {
    return Number.parseInt(localStorage.getItem('node_id')) + 1;
  }
  updateId() {
    localStorage.setItem('node_id', (this.getId() + 1).toString());
  }

  addNode() {
    const nodeactive = this.tree.treeModel.getActiveNode();
    const newjsonnode = {id: this.getId(), name: this.categoryName, description: this.categoryDescription, children: []};
    console.log(this.categoryName);
    const changeddatanode = this.tree.treeModel.getActiveNode().data.children.push(newjsonnode);
    this.tree.treeModel.setSelectedNode(nodeactive, changeddatanode);
    this.updateId();
    this.tree.treeModel.update();
    this.setState(this.tree.treeModel.getState());
    console.log(this.tree.treeModel.getState().activeNodeIds);
    this.nodes = this.tree.treeModel.nodes;
    localStorage.setItem('nodes', JSON.stringify(this.nodes));
  }

  addRootNode() {
    this.tree.treeModel.nodes.push({ id: this.getId(), name: this.categoryName, description: this.categoryDescription, children: [] });
    this.tree.treeModel.update();
    this.updateId();
    this.nodes = this.tree.treeModel.nodes;
    localStorage.setItem('nodes', JSON.stringify(this.nodes));
  }

  operationRequired() {
    return this.tree.treeModel.getActiveNode() == null;
  }

  deleteNode() {
    const nodetobedeleted = this.tree.treeModel.getActiveNode();
    _.remove(nodetobedeleted.parent.data.children, nodetobedeleted.data);
    this.nodes = this.tree.treeModel.nodes;
    console.log(this.nodes);
    localStorage.setItem('nodes', JSON.stringify(this.nodes));
    this.tree.treeModel.update();
    this.tempdesc = '';
    this.tempname = '';
    this.isSelected = false;
  }

  shouldShowDescription() {
    return this.isSelected;
  }

  shouldShowBtn() {
    return this.showingBtns;
  }

  showBtns() {
    this.showingBtns = this.categoryName !== '';
  }

  updateNode() {
    this.tree.treeModel.getActiveNode().data.name = this.categoryName;
    this.tree.treeModel.getActiveNode().data.description = this.categoryDescription;
    this.tree.treeModel.update();
    this.tempname = this.categoryName;
    this.tempdesc = this.categoryDescription;
    localStorage.setItem('nodes', JSON.stringify(this.tree.treeModel.nodes));
  }

  addChosenNode() {
    if (this.tree.treeModel.getActiveNode() != null) {
      this.addNode();
    } else {
      this.addRootNode();
    }
  }
}
